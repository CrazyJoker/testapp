/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('testapp')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
